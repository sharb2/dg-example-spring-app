package testApp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;

@RestController
public class StatusController {
    private String status;

    @RequestMapping("/")
    public HashMap<String, String> StatusRequest()
    {
        //test comment forward
        HashMap<String, String> status_map = new HashMap<String, String>();
        status_map.put("Status", "Success!");
        return status_map;
    }

    public void setStatus(String status){
        this.status  = status;
    }

    public String getStatus(){
        return status;
    }
}